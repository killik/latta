<?php

namespace App\Rules\Permission\Create;

use App\Lingu\Abstracts\Rules\Create\UniqueOnGuard as Rule;
use Spatie\Permission\Models\Permission;

class UniqueOnGuard extends Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return !Permission::whereName($value)->whereGuardName($this->__guard)->exists();
    }
}
