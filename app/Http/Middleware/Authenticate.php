<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Arr;

class Authenticate extends Middleware
{
    protected array $__guards;

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return !in_array('operator', $this->__guards) ? route('login') : route('auth.operator.login');
        }
    }

    protected function unauthenticated($request, array $guards)
    {
        $this->__guards = $guards;

        parent::unauthenticated($request, $guards);
    }
}
