<?php

namespace App\Http\Controllers\Dashboard\Operator;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\AssignRequest;
use App\Http\Requests\Dashboard\Role\RemoveRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Operator $operator): View
    {
        $roles = $operator->roles()->paginate(4);

        return view('dashboard.operator.role.index', compact('operator', 'roles'));
    }

    public function add(Operator $operator): View
    {
        $roles = Role::whereDoesntHave('users', fn ($query) => $query->whereId($operator->id))->paginate(4);

        return view('dashboard.operator.role.add', compact('roles', 'operator'));
    }

    public function assign(Operator $operator, Role $role, AssignRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $operator->assignRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.operator.role.assigned'))];

        return redirect(route('operator.roles', $operator->id))->with($message);
    }

    public function remove(Operator $operator, Role $role, RemoveRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $operator->removeRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.operator.role.removed'))];

        return redirect(route('operator.roles', $operator->id))->with($message);
    }
}
