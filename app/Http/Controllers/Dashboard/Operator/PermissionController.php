<?php

namespace App\Http\Controllers\Dashboard\Operator;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Permission\GiveRequest;
use App\Http\Requests\Dashboard\Permission\RemoveRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index(Operator $operator): View
    {
        $permissions = $operator->permissions()->paginate(4);

        return view('dashboard.operator.permission.index', compact('operator', 'permissions'));
    }

    public function add(Operator $operator): View
    {
        $permissions = Permission::whereDoesntHave('users', fn ($query) => $query->whereId($operator->id))->paginate(4);

        return view('dashboard.operator.permission.add', compact('permissions', 'operator'));
    }

    public function give(Operator $operator, Permission $permission, GiveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $operator->givePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.operator.permission.gived'))];

        return redirect(route('operator.permissions', $operator->id))->with($message);
    }

    public function remove(Operator $operator, Permission $permission, RemoveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $operator->revokePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.operator.permission.removed'))];

        return redirect(route('operator.permissions', $operator->id))->with($message);
    }
}
