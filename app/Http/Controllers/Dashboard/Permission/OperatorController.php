<?php

namespace App\Http\Controllers\Dashboard\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Permission\RemoveRequest;
use App\Http\Requests\Dashboard\Permission\GiveRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;

class OperatorController extends Controller
{
    public function index(Permission $permission): View
    {
        $operators = $permission->users()->paginate(4);

        return view('dashboard.permission.operator.index', compact('permission', 'operators'));
    }

    public function add(Permission $permission): View
    {
        $operators = Operator::whereDoesntHave('permissions', fn ($query) => $query->whereId($permission->id))->paginate(4);

        return view('dashboard.permission.operator.add', compact('permission', 'operators'));
    }

    public function give(Permission $permission, Operator $operator, GiveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $operator->givePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.operator.permission.assigned'))];

        return redirect(route('permission.operators', $permission->id))->with($message);
    }

    public function remove(Permission $permission, Operator $operator, RemoveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $operator->revokePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.operator.permission.removed'))];

        return redirect(route('permission.operators', $permission->id))->with($message);
    }

}
