<?php

namespace App\Http\Controllers\Dashboard\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\AssignRequest;
use App\Http\Requests\Dashboard\Role\RemoveRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Permission $permission): View
    {
        $roles = $permission->roles()->paginate(4);

        return view('dashboard.permission.role.index', compact('permission', 'roles'));
    }

    public function add(Permission $permission): View
    {
        $roles = Role::whereDoesntHave('permissions', fn ($query) => $query->whereId($permission->id))->paginate(4);

        return view('dashboard.permission.role.add', compact('roles', 'permission'));
    }

    public function assign(Permission $permission, Role $role, AssignRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $permission->assignRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.permission.role.assigned'))];

        return redirect(route('permission.roles', $permission->id))->with($message);
    }

    public function remove(Permission $permission, Role $role, RemoveRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $permission->removeRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.permission.role.removed'))];

        return redirect(route('permission.roles', $permission->id))->with($message);
    }
}
