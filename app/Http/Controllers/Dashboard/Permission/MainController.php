<?php

namespace App\Http\Controllers\Dashboard\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Permission\DestroyRequest;
use App\Http\Requests\Dashboard\Permission\PatchRequest;
use App\Http\Requests\Dashboard\Permission\StoreRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;

class MainController extends Controller
{
    public function index(): View
    {
        $permissions = Permission::orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.permission.index', compact('permissions'));
    }

    public function show(Permission $permission): View
    {
        return view('dashboard.permission.show', compact('permission'));
    }

    public function create(): View
    {
        $permissions = Permission::whereGuardName('permission')->get()->all();

        return view('dashboard.permission.form.create', compact('permissions'));
    }

    public function store(StoreRequest $req): RedirectResponse
    {
        Permission::create($req->all());

        return redirect(route('permission.index'))->with(['success' => trans('dashboard.permission.stored')]);
    }

    public function edit(Permission $permission): View
    {
        return view('dashboard.permission.form.edit', compact('permission'));
    }

    public function update(PatchRequest $req, Permission $permission): RedirectResponse
    {
        $permission->update($req->all());

        return redirect(route('permission.index'))->with(['success' => trans('dashboard.permission.updated')]);
    }

    public function destroy(DestroyRequest $req, Permission $permission): RedirectResponse
    {
        $permission = $req->route('permission');

        $permission->delete();

        return redirect(route('permission.index'))->with(['success' => trans('dashboard.permission.removed')]);
    }
}
