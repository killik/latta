<?php

namespace App\Http\Controllers\Dashboard\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\RemoveRequest;
use App\Http\Requests\Dashboard\Role\AssignRequest;
use App\Operator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class OperatorController extends Controller
{
    public function index(Role $role): View
    {
        $operators = $role->users()->paginate(4);

        return view('dashboard.role.operator.index', compact('role', 'operators'));
    }

    public function add(Role $role): View
    {
        $operators = Operator::whereDoesntHave('roles', fn ($query) => $query->whereId($role->id))->paginate(4);

        return view('dashboard.role.operator.add', compact('role', 'operators'));
    }

    public function assign(Role $role, Operator $operator, AssignRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $operator->assignRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.operator.role.assigned'))];

        return redirect(route('role.operators', $role->id))->with($message);
    }

    public function remove(Role $role, Operator $operator, RemoveRequest $req): RedirectResponse
    {
        $role = $req->route('role');

        $operator->removeRole($role->name);

        $message = ['success' => str_replace(':role', $role->name, trans('dashboard.operator.role.removed'))];

        return redirect(route('role.operators', $role->id))->with($message);
    }

}
