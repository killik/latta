<?php

namespace App\Http\Controllers\Dashboard\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Permission\GiveRequest;
use App\Http\Requests\Dashboard\Permission\RemoveRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index(Role $role): View
    {
        $permissions = $role->permissions()->paginate(4);

        return view('dashboard.role.permission.index', compact('role', 'permissions'));
    }

    public function add(Role $role): View
    {
        $permissions = Permission::whereDoesntHave('roles', fn ($query) => $query->whereId($role->id))->paginate(4);

        return view('dashboard.role.permission.add', compact('permissions', 'role'));
    }

    public function give(Role $role, Permission $permission, GiveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $role->givePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.role.permission.gived'))];

        return redirect(route('role.permissions', $role->id))->with($message);
    }

    public function remove(Role $role, Permission $permission, RemoveRequest $req): RedirectResponse
    {
        $permission = $req->route('permission');

        $role->revokePermissionTo($permission->name);

        $message = ['success' => str_replace(':permission', $permission->name, trans('dashboard.role.permission.removed'))];

        return redirect(route('role.permissions', $role->id))->with($message);
    }
}
