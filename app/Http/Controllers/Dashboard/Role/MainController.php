<?php

namespace App\Http\Controllers\Dashboard\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\DestroyRequest;
use App\Http\Requests\Dashboard\Role\PatchRequest;
use App\Http\Requests\Dashboard\Role\StoreRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class MainController extends Controller
{
    public function index(): View
    {
        $roles = Role::orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.role.index', compact('roles'));
    }

    public function show(Role $role): View
    {
        return view('dashboard.role.show', compact('role'));
    }

    public function create(): View
    {
        $roles = Role::whereGuardName('role')->get()->all();

        return view('dashboard.role.form.create', compact('roles'));
    }

    public function store(StoreRequest $req): RedirectResponse
    {
        Role::create($req->all());

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.stored')]);
    }

    public function edit(Role $role): View
    {
        return view('dashboard.role.form.edit', compact('role'));
    }

    public function update(PatchRequest $req, Role $role): RedirectResponse
    {
        $role->update($req->all());

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.updated')]);
    }

    public function destroy(DestroyRequest $req, Role $role): RedirectResponse
    {
        $role = $req->route('role');

        $role->delete();

        return redirect(route('role.index'))->with(['success' => trans('dashboard.role.removed')]);
    }
}
