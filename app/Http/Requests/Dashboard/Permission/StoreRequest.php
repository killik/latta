<?php

namespace App\Http\Requests\Dashboard\Permission;

use App\Rules\Permission\Create\UniqueOnGuard;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Guard;
use Spatie\Permission\Models\Permission;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('generate new permission');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $unique = new UniqueOnGuard($this->guard_name ?? Guard::getDefaultName(Permission::class));

        return [
            'name' => ['required', 'min:4', 'max:24', $unique],
            'guard_name' => 'nullable|min:3|max:24'
        ];
    }
}
