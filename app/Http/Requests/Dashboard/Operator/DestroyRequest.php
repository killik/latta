<?php

namespace App\Http\Requests\Dashboard\Operator;

use Illuminate\Foundation\Http\FormRequest;

class DestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $operator = $this->route('operator');

        return $this->user()->can('remove operator') && ($operator->name != 'super user' || $operator->guard_name != 'operator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
