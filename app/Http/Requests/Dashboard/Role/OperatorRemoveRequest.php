<?php

namespace App\Http\Requests\Dashboard\Role;

use Illuminate\Foundation\Http\FormRequest;

class OperatorRemoveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $role = $this->route('role');

        $operator = $this->route('operator');

        return $this->user()->can('remove role') && ($this->user()->username != $operator->username || $role->name != 'super user' || $role->guard_name != 'operator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
