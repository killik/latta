<?php

namespace App\Http\Requests\Dashboard\Role;

use Illuminate\Foundation\Http\FormRequest;

class PatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $role = $this->route('role');

        return $this->user()->can('update role') && ($role->name != 'super user' || $role->guard_name != 'operator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:roles,id|min:4|max:24',
            'guard_name' => 'nullable|min:3|max:24'
        ];
    }
}
