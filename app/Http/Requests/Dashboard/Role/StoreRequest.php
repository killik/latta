<?php

namespace App\Http\Requests\Dashboard\Role;

use App\Rules\Role\Create\UniqueOnGuard;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Guard;
use Spatie\Permission\Models\Role;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('generate new role');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $unique = new UniqueOnGuard($this->guard_name ?? Guard::getDefaultName(Role::class));

        return [
            'name' => ['required', 'min:4', 'max:24', $unique],
            'guard_name' => 'nullable|min:3|max:24'
        ];
    }
}
