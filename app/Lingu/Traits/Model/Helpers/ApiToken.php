<?php

namespace App\Lingu\Traits\Model\Helpers;

use Illuminate\Support\Str;

trait ApiToken
{
    protected function generateApiToken(int $length = 100): void
    {
        $api_token = Str::random($length);

        if ($this->where('api_token', $this->api_token)->exists()) {
            call_user_func([$this, __FUNCTION__], $length);
        }

        else $this->api_token = $api_token;
    }
}
