<?php

namespace App\Lingu\Traits\Model\Relations\HasOwner;

use App\User;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasOneUser {
    public function owner(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
