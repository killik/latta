<?php

namespace App\Console\Commands;

use App\Operator;
use Illuminate\Console\Command;
use Spatie\Permission\Exceptions\RoleDoesNotExist;

class OperatorSuper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operator:super {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign super user role to operator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = strtolower($this->argument("username"));

        $user = Operator::whereUsername($username)->get()->first();

        if (!$user) $this->error(sprintf("username: '%s' not found", $username));

        else try {
            $user->assignRole("super user");

            $this->info(sprintf("Operator %s (%s) now is super user", $user->name, $user->username));
        }

        catch (RoleDoesNotExist $e) {
            $this->error($e->getMessage());
        }

    }
}
