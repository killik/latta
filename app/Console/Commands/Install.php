<?php

namespace App\Console\Commands;

use App\Operator;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Install extends Command
{
    protected string $__version = '1.0.0';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdd:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install this app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (is_file(base_path('version'))) $this->__version = file_get_contents(base_path('version'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $this->__logo();

        $this->info(trans('install.welcome'));

        sleep(1);

        $this->warn(trans('install.warning'));

        sleep(1);

        if ($this->confirm(trans('install.confirm'))) {
            $this->output->section(trans('install.database'));

            sleep(1);

            $this->comment(trans('install.database_comment'));

            try {
                $this->call('migrate:fresh', ['--seed' => 'default']);
            }

            catch (Exception $e) {
                $this->error($e->getMessage());
            }

            $this->output->section(trans('install.operator'));

            sleep(1);

            try {
                Operator::create([
                    'username'  => $this->__inputUserName(),
                    'name'      => $this->__inputName(),
                    'email'     => $this->__inputEmail(),
                    'password'  => $this->__inputPassword()
                ])

                ->assignRole('super user');
            }

            catch (Exception $e) {
                $this->error($e->getMessage());
            }
        }

        else $this->info(trans('install.backup'));
    }

    public function __inputUserName(): string
    {
        $error      = false;
        $userName   = $this->ask(trans('install.ask_username'));

        if (preg_match('/[^A-Za-z0-9]/', $userName)) {
            $error = sprintf(trans('install.username_eformat'), 'A-Za-z0-9');
        }

        if (($len = Str::length($userName)) > 8 || $len < 4) {
            $error = sprintf(trans('install.username_elength'), '4', '8');
        }

        return !$error ? $userName : call_user_func([$this, __FUNCTION__], $this->error($error));
    }

    public function __inputName(): string
    {
        $error  = false;
        $name   = $this->ask(trans('install.ask_name'));

        if (preg_match('/[^A-Za-z\s]/', $name)) {
            $error = sprintf(trans('install.name_eformat'), 'A-Za-z');
        }

        if (($len = Str::length($name)) > 30 || $len < 3) {
            $error = sprintf(trans('install.name_elength'), '3', '30');
        }

        return !$error ? $name : call_user_func([$this, __FUNCTION__], $this->error($error));
    }

    public function __inputEmail(): string
    {
        $email  = $this->ask(trans('install.ask_email'));

        $error  = filter_var($email, FILTER_VALIDATE_EMAIL) ? false : trans('install.email_eformat');

        return !$error ? $email : call_user_func([$this, __FUNCTION__], $this->error($error));
    }

    public function __inputPassword(): string
    {
        $password   = $this->ask(trans('install.ask_password'));

        $error      = Str::length($password) < 4 ? trans('install.password_eshort') : false;

        return !$error ? bcrypt($password) : call_user_func([$this, __FUNCTION__], $this->error($error));
    }

    protected function __logo(): void
    {
        $pdd = '4pab4paA4paWICAgICAg4paQICAgICDilpwgIOKWm+KWgOKWliAgICAgICAgICDilpviloDilpbilpcgICAg4paX4paQICAgICDilpwgCuKWmeKWhOKWmOKWnuKWgOKWluKWmeKWgOKWluKWnOKWgCDilp3iloDilpbilpAgIOKWjCDilozilp7iloDilpbilp7iloDilpjilp3iloDilpYg4paMIOKWjOKWhCDilp7iloDiloziloTilpziloAg4pad4paA4paW4paQIArilowgIOKWjCDilozilowgIOKWkCDilpbilp7iloDilozilpAgIOKWjCDilozilpviloAg4pad4paA4paW4pae4paA4paMIOKWjCDilozilpAg4paa4paE4paM4paQ4paQIOKWluKWnuKWgOKWjOKWkCAK4paYICDilp3iloAg4paYICAg4paAIOKWneKWgOKWmCDilpgg4paA4paAIOKWneKWgOKWmOKWgOKWgCDilp3iloDilpgg4paA4paAIOKWgOKWmOKWl+KWhOKWmOKWgOKWmOKWgCDilp3iloDilpgg4paY';

        system('clear');

        $this->line(base64_decode($pdd).$this->__version);
    }
}
