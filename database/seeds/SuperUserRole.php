<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SuperUserRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Role::create(['name' => 'super user', 'guard_name' => 'operator']);
    }
}
