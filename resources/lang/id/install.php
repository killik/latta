<?php

return [
    'warning' => 'Tindakan ini akan melakukan pemasangan dari awal dan akan menghapus semua data di dalam basis data (jika ada)',
    'confirm' => 'Yakin untuk melanjutkan?',
    'welcome' => 'Selamat datang di pemasang Portal Desa Digital',
    'backup' => 'Cadangkan basis data anda :)',
    'database' => 'Melakukan pengaturan basis data',
    'database_comment' => 'Mungkin akan membutuhkan waktu beberapa menit.',
    'operator' => 'Membuat akun Operator'

];
