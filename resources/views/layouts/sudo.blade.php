@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4 pb-4">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-shield-alt"></i>
                    Super User Menu
                </div>
                <div class="card-body list-group p-0 py-2">
                    <a href="{{ route('operator.index') }}" class="list-group-item text-decoration-none text-dark border-0 py-1">
                        <i class="fa fa-user"></i>
                        Operators
                    </a>
                    <a href="{{ route('permission.index') }}" class="list-group-item text-decoration-none text-dark border-0 py-1">
                        <i class="fa fa-frog"></i>
                        Permissions
                    </a>
                    <a href="{{ route('role.index') }}" class="list-group-item text-decoration-none text-dark border-0 py-1">
                        <i class="fa fa-hashtag"></i>
                        Roles
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-@yield('icon')"></i>
                    @yield('title')
                </div>
                @yield('body')
            </div>
        </div>
    </div>
</div>
@endsection
