@extends('layouts.sudo')

@section('icon', 'frog')
@section('title')
    Permission:
    <a href="{{ route('permission.show', $permission->id) }}">
        <span class="badge badge-primary">
            {{ $permission->name }}
        </span>
    </a>
    <a href="{{ route('permission.roles', $permission->id) }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('role')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <thead>
                <tr>
                    <td><i class="fa fa-wheelchair"></i></td>
                    <td><i class="fa fa-signature"></i> Name</td>
                    <td><i class="fa fa-user-shield"></i> Guard</td>
                    <td><i class="fa fa-fire-alt"></i> Aksi</td>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @forelse ($roles as $row)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>
                        <a href="{{ route('role.show', $row->id) }}" class="badge badge-{{ $row->name == 'super user' && $row->guard_name == 'permission'?'danger':'primary' }}">
                            {{ $row->name }}
                        </a>
                    </td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $row->guard_name }}
                        </span>
                    </td>
                    <td>
                        <form action="{{ route('permission.roles.assign', [$permission->id, $row->id]) }}" method="POST">
                            @csrf
                            <input type="hidden" name="role" value="{{ $row->name }}"/>
                            <a class="btn btn-secondary btn-sm" href="{{ route('role.show', $row->id) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <button class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {!! $roles->links() !!}
</div>
@endsection
