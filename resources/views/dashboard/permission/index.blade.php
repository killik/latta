@extends('layouts.sudo')

@section('icon', 'frog')
@section('title')
    Permissions
    <a href="{{ route('permission.create') }}" class="float-right text-dark">
        <i class="fa fa-edit"></i> Create
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('permission')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <thead>
                <tr>
                    <td><i class="fa fa-wheelchair"></i></td>
                    <td><i class="fa fa-signature"></i> Name</td>
                    <td><i class="fa fa-user-shield"></i> Guard</td>
                    <td><i class="fa fa-fire-alt"></i> Aksi</td>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @forelse ($permissions as $row)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>
                        <span class="badge badge-{{ $row->name == 'super user' && $row->guard_name == 'operator'?'danger':'primary' }}">
                            {{ $row->name }}
                        </span>
                    </td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $row->guard_name }}
                        </span>
                    </td>
                    <td>
                        <form action="{{ route('permission.destroy', $row->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="role" value="{{ $row->name }}"/>
                            <a class="btn btn-primary btn-sm" href="{{ route('permission.edit', $row->id) }}">
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                            <a class="btn btn-secondary btn-sm" href="{{ route('permission.show', $row->id) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {!! $permissions->links() !!}
</div>
@endsection
