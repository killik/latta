@extends('layouts.sudo')

@section('icon', 'user')
@section('title')
    Operator
    <a href="{{ route('operator.index') }}" class="float-right text-decoration-none text-dark">
        <i class="fa fa-arrow-left"></i> Back
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('role')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <tbody>
                <tr>
                    <td class="text-left"><i class="fa fa-calendar-alt"></i> Created At</td>
                    <td>{{ $operator->created_at }}</td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-signature"></i> Name</td>
                    <td>
                        {{ $operator->name }}
                    </td>
                </tr>
                <tr>
                    <td class="text-left"><i class="fa fa-user-shield"></i> User Name</td>
                    <td>
                        <span class="badge badge-{{ $operator->hasRole('super user') ? 'danger':'primary' }}">
                            {{ $operator->username }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <a href="{{ route('operator.roles', $operator->id) }}" class="text-dark text-decoration-none">
                            <i class="fa fa-hashtag"></i> Roles
                        </a>
                    </td>
                    <td class="text-wrap">
                        @forelse ($operator->roles as $role)
                            <a href="{{ route('role.show', $role->id) }}" class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
                                {{ $role->name }}
                            </a>
                        @empty
                            <span class="badge badge-danger">
                                no role
                            </span>
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <a href="{{ route('operator.permissions', $operator->id) }}" class="text-dark text-decoration-none">
                            <i class="fa fa-frog"></i> Permissions
                        </a>
                    </td>
                    <td class="text-wrap">
                        @if ($operator->hasRole('super user'))
                            <span class="badge badge-danger">
                                all permissions
                            </span>
                        @else
                            @forelse ($operator->permissions as $permission)
                                <a href="{{ route('permission.show', $permission->id) }}" class="badge badge-primary">
                                    {{ $permission->name }}
                                </a>
                            @empty
                                <span class="badge badge-danger">
                                    no permissions
                                </span>
                            @endforelse
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
