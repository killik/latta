@extends('layouts.sudo')

@section('icon', 'user')
@section('title')
    Role:
    <a href="{{ route('role.show', $role->id) }}">
        <span class="badge badge-{{ $role->name == 'super user' && $role->guard_name == 'operator'?'danger':'primary' }}">
            {{ $role->name }}
        </span>
    </a>
    <a href="{{ route('role.operators.add', $role->id) }}" class="float-right text-dark">
        <i class="fa fa-plus"></i> Add
    </a>
@endsection

@section('body')
<div class="card-body pb-0 pt-1 text-center">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @error ('operator')

        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="table-responsive">
        <table class="table table-hover table-sm table-borderless text-nowrap">
            <thead>
                <tr>
                    <td><i class="fa fa-wheelchair"></i></td>
                    <td><i class="fa fa-user-shield"></i> Username</td>
                    <td><i class="fa fa-envelope"></i> Email</td>
                    <td><i class="fa fa-fire-alt"></i> Aksi</td>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @forelse ($operators as $row)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>
                        <a class="badge badge-{{ $row->hasRole('super user') ? 'danger':'primary' }}" href="{{ route('operator.show', $row->id) }}">
                            {{ $row->username }}
                        </a>
                    </td>
                    <td>
                        <span class="badge badge-secondary">
                            {{ $row->email }}
                        </span>
                    </td>
                    <td>
                        <form action="{{ route('role.operators.remove', [$role->id, $row->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="role" value="{{ $row->name }}"/>
                            <a class="btn btn-primary btn-sm" href="{{ route('operator.show', $row->id) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {!! $operators->links() !!}
</div>
@endsection
