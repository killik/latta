<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', fn () => view('welcome'));

Route::group(['prefix' => 'auth'], base_path('routes/web/auth.php'));
Route::group(['prefix' => 'dashboard'], base_path('routes/web/dashboard.php'));

Route::get('/home', 'HomeController@index')->name('home');
