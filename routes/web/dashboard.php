<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Dashboard', 'middleware' => 'auth:operator'], function () {
    Route::get('/', fn () => view('dashboard.index'))->name('dashboard');

    Route::group(['prefix' => 'sudo', 'middleware' => 'role:super user'], function () {
        Route::get('/', fn () => redirect(route('operator.index')));

        Route::group(['prefix' => 'operator'], base_path('routes/web/dashboard/operator.php'));
        Route::group(['prefix' => 'role'], base_path('routes/web/dashboard/role.php'));
        Route::group(['prefix' => 'permission'], base_path('routes/web/dashboard/permission.php'));
    });
});
