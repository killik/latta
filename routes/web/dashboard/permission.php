<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Permission'], function () {
    Route::get('/', 'MainController@index')->name('permission.index');
    Route::get('create', 'MainController@create')->name('permission.create');

    Route::post('/', 'MainController@store')->name('permission.store');

    Route::group(['prefix' => '{permission}'], function () {
        Route::get('/', 'MainController@show')->name('permission.show');
        Route::get('edit', 'MainController@edit')->name('permission.edit');

        Route::delete('/', 'MainController@destroy')->name('permission.destroy');

        Route::patch('/', 'MainController@update')->name('permission.update');

        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@index')->name('permission.roles');
            Route::get('add', 'RoleController@add')->name('permission.roles.add');

            Route::group(['prefix' => '{role}'], function () {
                Route::post('assign', 'RoleController@assign')->name('permission.roles.assign');

                Route::delete('remove', 'RoleController@remove')->name('permission.roles.remove');
            });
        });

        Route::group(['prefix' => 'operator'], function () {
            Route::get('/', 'OperatorController@index')->name('permission.operators');
            Route::get('add', 'OperatorController@add')->name('permission.operators.add');

            Route::group(['prefix' => '{operator}'], function () {
                Route::post('give', 'OperatorController@give')->name('permission.operators.give');

                Route::delete('remove', 'OperatorController@remove')->name('permission.operators.remove');
            });
        });
    });
});
