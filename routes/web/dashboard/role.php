<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Role'], function () {
    Route::get('/', 'MainController@index')->name('role.index');
    Route::get('create', 'MainController@create')->name('role.create');

    Route::post('/', 'MainController@store')->name('role.store');

    Route::group(['prefix' => '{role}'], function () {
        Route::get('/', 'MainController@show')->name('role.show');
        Route::get('edit', 'MainController@edit')->name('role.edit');

        Route::delete('/', 'MainController@destroy')->name('role.destroy');

        Route::patch('/', 'MainController@update')->name('role.update');

        Route::group(['prefix' => 'operator'], function () {
            Route::get('/', 'OperatorController@index')->name('role.operators');
            Route::get('add', 'OperatorController@add')->name('role.operators.add');

            Route::group(['prefix' => '{operator}'], function () {
                Route::post('assign', 'OperatorController@assign')->name('role.operators.assign');

                Route::delete('remove', 'OperatorController@remove')->name('role.operators.remove');
            });
        });

        Route::group(['prefix' => 'permission'], function () {
            Route::get('/', 'PermissionController@index')->name('role.permissions');
            Route::get('add', 'PermissionController@add')->name('role.permissions.add');

            Route::group(['prefix' => '{permission}'], function () {
                Route::post('give', 'PermissionController@give')->name('role.permissions.give');

                Route::delete('remove', 'PermissionController@remove')->name('role.permissions.remove');
            });
        });
    });
});
