<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Operator'], function () {
    Route::get('/', 'MainController@index')->name('operator.index');
    Route::get('create', 'MainController@create')->name('operator.create');

    Route::post('/', 'MainController@store')->name('operator.store');

    Route::group(['prefix' => '{operator}'], function () {
        Route::get('/', 'MainController@show')->name('operator.show');
        Route::get('edit', 'MainController@edit')->name('operator.edit');

        Route::delete('/', 'MainController@destroy')->name('operator.destroy');

        Route::patch('/', 'MainController@update')->name('operator.update');

        Route::group(['prefix' => 'roles'], function () {
            Route::get('/', 'RoleController@index')->name('operator.roles');
            Route::get('add', 'RoleController@add')->name('operator.roles.add');

            Route::group(['prefix' => '{role}'], function () {
                Route::post('assign', 'RoleController@assign')->name('operator.roles.assign');

                Route::delete('remove', 'RoleController@remove')->name('operator.roles.remove');
            });
        });

        Route::group(['prefix' => 'permissions'], function () {
            Route::get('/', 'PermissionController@index')->name('operator.permissions');
            Route::get('add', 'PermissionController@add')->name('operator.permissions.add');

            Route::group(['prefix' => '{permission}'], function () {
                Route::post('give', 'PermissionController@give')->name('operator.permissions.give');

                Route::delete('remove', 'PermissionController@remove')->name('operator.permissions.remove');
            });
        });
    });
});
